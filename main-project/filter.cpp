#include "filter.h"
#include "Exchange_rates.h"
#include <cstring>
#include <iostream>

Exchange_rates** filter(Exchange_rates* array[], int size, bool(*check)(Exchange_rates* element), int& result_size) {
	Exchange_rates** result = new Exchange_rates * [size];
	result_size = 0;
	for (int i = 0; i < size; i++) {
		if (check(array[i])) {
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_by_exam(Exchange_rates* element) {
	return strcmp(element -> nazv.nazv, "�����������") == 0;
}

bool check_by_mark(Exchange_rates* element) {
	return element->per.poc < 2.5;
}