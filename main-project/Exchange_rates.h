#ifndef EXCHANGE_RATES_H
#define EXCHANGE_RATES_H

#include "constants.h"

struct summa
{
    int poc;
    int prod;
};


struct bank
{
    char nazv[MAX_STRING_SIZE];
};


struct Exchange_rates
{
    bank nazv;
    summa per;
    summa vt;
    char adres[MAX_STRING_SIZE];
};

#endif
