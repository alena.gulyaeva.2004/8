#include "nazv.h"
#include "constants.h"

#include <fstream>
#include <cstring>

summa convert(char* str)
{
    summa result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.poc = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.prod = atoi(str_number);
    return result;
}

void read(const char* file_name, Exchange_rates* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            Exchange_rates* item = new Exchange_rates;
            file >> item->nazv.nazv;
            file >> tmp_buffer;
            item->per = convert(tmp_buffer);
            file >> tmp_buffer;
            item->vt = convert(tmp_buffer);
            file.getline(item->adres, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}