#ifndef FILTER_H
#define FILTER_H

#include "Exchange_rates.h"

Exchange_rates** filter(Exchange_rates* array[], int size, bool (*check)(Exchange_rates* element), int& result_size);

/*
  <function_name>:
              ,
          true,
    ,

:
    array       -
    size        -
    check       -    .

                   ,
    result_data - ,    - ,



          ,
     (     true)
*/


bool check_book_subscription_by_author(Exchange_rates* element);

/*
  check_book_subscription_by_author:
      - ,

:
    element -   ,


    true,           ,  false
*/


bool check_book_subscription_by_date(Exchange_rates* element);

/*
  check_book_subscription_by_date:
      - ,           2021-

:
    element -   ,


    true,           2021- ,  false
*/

#endif

