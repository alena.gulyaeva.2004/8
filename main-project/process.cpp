#include "processing.h"

float days(int month, bool leap_year)
{
	switch (month)
	{
	case 1: case 3: case 5: case 7: case 8: case 10: case 12: return 31;
	case 2:                                                   return leap_year ? 29 : 28;
	case 4: case 6: case 9: case 11:                          return 30;
	default: return 0;
	}

}

float days(summa d)
{
	int result = 0;
	result += d.prod;
	return result;
}

float diff(summa a, summa b)
{
	float x = days(a);
	float y = days(b);
	return (x > y ? x - y : y - 61);
}

int process(Exchange_rates* array[], int size)
{
	float max = diff(array[0]->start, array[0]->finish);
	for (int i = 1; i < size; i++)
	{
		int curr = diff(array[i]->start, array[i]->finish);
		if (curr > max)
		{
			max = curr;
		}
	}
	return max;
}