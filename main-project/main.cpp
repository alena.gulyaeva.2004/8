#include <iostream>
#include <iomanip>

using namespace std;

#include "Exchange_rates.h"
#include "nazv.h"
#include <iostream>
#include <iomanip>

using namespace std;

#include "Exchange_rates.h"
#include "nazv.h"
#include "constants.h"
#include "filter.h"
#include "processing.h"

void output(Exchange_rates* subscription)
{
	/********** ����� �������� **********/
	cout << "��������........: ";
	// ����� �������
	cout << subscription->nazv.nazv << " ";
	/********** ����� ����� **********/

	cout << '"' << subscription->adres << '"';
	cout << '\n';
	/********** ����� ���� ������ **********/
	// ����� ����
	cout << "���� ������.....: ";
	cout << setw(4) << setfill('0') << subscription->per.poc << '-';
	// ����� ������
	cout << setw(2) << setfill('0') << subscription->per.prod << '-';
	cout << '\n';
	/********** ����� ���� �������� **********/
	// ����� ����
	cout << "���� ��������...: ";
	cout << setw(4) << setfill('0') << subscription->vt.poc << '-';
	// ����� ������
	cout << setw(2) << setfill('0') << subscription->vt.prod << '-';
	cout << '\n';
	cout << '\n';
}

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "������������ ������ �8. GIT\n";
	cout << "������� �0. ������������ ���������\n";
	cout << "�����: ������ ���������\n";
	cout << "������: XX\n\n";
	Exchange_rates* subscriptions[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", subscriptions, size);
		cout << "***** ������������ ��������� *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(subscriptions[i]);
		}
		bool (*check_function)(Exchange_rates*) = NULL; // check_function - ��� ��������� �� �������, ������������ �������� ���� bool,
														   // � ����������� � �������� ��������� �������� ���� book_subscription*
		cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
		cout << "1) ������ ���������� � ������� ������� ���������� ����������\n";
		cout << "2) ������ ���������� � ����� ������ � ����� 2015-�� ����\n";
		cout << "3) ������������ ���������� ����, �� ������� ����� ���� ������ �������\n";
		cout << "\n������� ����� ���������� ������: ";
		int item;
		cin >> item;
		cout << '\n';
		if (check_function)
		{
			int new_size;
			Exchange_rates** filtered = filter(subscriptions, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete subscriptions[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}
